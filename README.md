# Installation

1. Open a terminal, cd to the folder:

```
cd cctv-stream-server
```

2. Install all the dependencies:

```
npm install
```

3. Open file .env-sample and fill in the values of dahua camera IP address, port, username, and password
4. Rename .env-sample to .env

```
mv .env-sample .env
```

5. Download and install pre-build FFMPEG Builds on your local machine (Download Build)
   FFmpeg - Multimedia framework to decode, encode, transcode, mux, demux, stream, filter and play
   Copy the FFMPEG Zip folder you have just downloaded, paste it into C: drive for simplicity and unzip it.
   Rename the file to ffmpeg for simpicity
   After unzipped the file, navigate ffmpeg/bin and add as System Environment Variable

6. Run index.js from terminal:

```
node index.js
```
