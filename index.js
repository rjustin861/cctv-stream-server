const Stream = require('node-rtsp-stream');

require('dotenv').config();

//Camera Authentication
const host = process.env.DAHUA_HOST;
const port = process.env.DAHUA_PORT;
const username = process.env.DAHUA_USER;
const password = process.env.DAHUA_PASSWORD;
const feed_channel = process.env.FEED_CHANNEL || 1;

//A channel of camera stream
stream = new Stream({
  streamUrl:
    'rtsp://' +
    username +
    ':' +
    password +
    '@' +
    host +
    `:${port}/cam/realmonitor?channel=${feed_channel}&subtype=1`,
  wsPort: 9009,
  width: 800,
  height: 600,
  ffmpegOptions: {
    '-s': '800x600'
  }
});
